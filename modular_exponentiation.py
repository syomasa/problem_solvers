def modular_exponentiatiation(number, power, mod):
    """
    this performs modular exponentiation into
    a specific modular
        param int n: number you need to perform modular exponentiation
        param int power: exponent of number
        param int mod: number in which mod number is belonging
    """

    exponent = 1 # start calculating exponents from 1
    exponents = [] # list of exponents that are used
    while True:
        if exponent > power: # check is exponent too big
            break
        exponents.append(exponent) # add exponent to exponents list
        exponent *= 2

    mod_values = [] # list for storing values of specified mod
    for i in exponents:
        if number ** i <= mod:
            mod_values.append(number**i)

        else:
            remain = (number**i) % mod
            mod_values.append(remain)

    # data presentation
    print("Your values in mod {}:".format(mod))
    for i, value in enumerate(mod_values):
        print("{}^{} = {} mod {}".format(number, exponents[i], value, mod))

if __name__ == "__main__":
    while True:
        try:
            number = int(input("Give number: "))
            power = int(input("Give power: "))
            mod = int(input("Give mod: "))
            print()
        except ValueError:
            print("Only integers are allowed")
        else:
            modular_exponentiatiation(number, power, mod)
            break
    input()
