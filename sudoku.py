import numpy
import os
import sys
from time import sleep
#from IPython.display import clear_output

grid = [[0, 0 ,0, 0, 6, 7, 5, 8, 0],
        [8, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 6, 7, 9, 0 ,0 ,0, 3, 0],
        [3, 8, 0, 0, 0, 0, 9, 0, 6],
        [2, 0, 9, 6, 0, 5, 7, 0, 3],
        [6, 0, 4, 0, 0, 0, 0, 5, 8],
        [0, 4, 0, 0, 0, 6, 3, 2, 0],
        [0, 0, 0, 0, 2, 0, 0, 0, 9],
        [0, 2, 1, 3, 7, 0, 0, 0, 0]]

def clear():
    # tells the system to run clear command (not certain is it workin on windows)
    os.system('clear')

def is_it_possible(pos_x, pos_y, n):

    # checks is number n on x-axis if not executing function continues
    for i in range(9):
        if grid[pos_y][i] == n:
            return False
    # check is number n on y-axis if not executing function continues
    for i in range(9):
        if grid[i][pos_x] == n:
            return False
    # checks is number n already on region if not executing function continues
    x0 = (pos_x // 3) * 3
    y0 = (pos_y // 3) * 3
    for i in range(0, 3):
        for j in range(0, 3):
            if grid[y0 + i][x0 + j] == n:
                return False
    # if none of the conditions before are true returns true 
    return True

def solve():
    # starts looping through the game field
    for y in range(9):
        for x in range(9):
            clear()
            # check is the cell already solved 
            if grid[y][x] == 0: 
                # start looping through numbers 1-9 
                for n in range(1, 10):
                    if is_it_possible(x, y, n):
                        
                        grid[y][x] = n
                        solve() # using recursion to determine next possible entry
                        grid[y][x] = 0 # if the recursion is unable to find proper solution backtrack back to zero    
                return
            print(numpy.matrix(grid))
            sleep(0) 
    # program stops once the first correct sollution is found out
    # user is able to choose if he/she wants to find out other possible solutions 
    prompt = input("More solutions (if there is any)? (n to exit) ") 
    
    # checks if the user wants to quit
    if prompt == 'n':
        sys.exit()

if __name__ == '__main__':
    solve()

